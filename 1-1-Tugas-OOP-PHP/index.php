<?php

trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        echo $this->nama." sedang ".$this->keahlian;
    }
}

abstract class Fight{
    use Hewan;
    public $attackPower;
    public $deffencePower;

    public function serang($hewan){
        echo $this->nama." sedang menyerang ". $hewan->nama."<br>";
        $hewan->diserang($this);
    }

    public function diserang($hewan){
        echo $this->nama." sedang diserang ". $hewan->nama;
        $this->darah = $this->darah - $hewan->attackPower/$this->deffencePower;
    }

    protected function get_info(){
        echo "<br>";
        echo "Nama hewan : ".$this->nama;
        echo "<br>";
        echo "Darah hewan : ".$this->darah;
        echo "<br>";
        echo "Jumlah kaki hewan : ".$this->jumlahKaki;
        echo "<br>";
        echo "Keahlian hewan : ".$this->keahlian;
        echo "<br>";
        echo "Attack Power : ".$this->attackPower;
        echo "<br>";
        echo "Deffence Power : ".$this->deffencePower;
        echo "<br>";
        $this->atraksi();
    }
    abstract public function getInfoHewan();

}

class Elang extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->deffencePower = 5;
    }
    public function getInfoHewan(){
        echo "Detail Hewan Elang : ";
        echo "<br>";
        $this->get_info();
    }
}

class Harimau extends Fight{
    public function __construct($nama){
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->deffencePower = 8;
    }
    
    public function getInfoHewan(){
        echo "Detail Hewan Harimau : ";
        echo "<br>";
        $this->get_info();
    }
}

$elang = new Elang('elang');
$elang->getInfoHewan();
echo "<br>";
echo "===============";
echo "<br>";
$harimau = new Harimau('harimau');
$harimau->getInfoHewan();
echo "<br>";
$elang->serang($harimau);
echo $harimau->getInfoHewan();
?>
