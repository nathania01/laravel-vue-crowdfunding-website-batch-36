const luasPersegiPanjang = (panjang,lebar) => {
    return panjang*lebar
}

const kelilingPersegiPanjang = (panjang,lebar)=>{
    return 2*(panjang+lebar)
}

console.log(luasPersegiPanjang(10,20))
console.log(kelilingPersegiPanjang(10,20))

//Soal 2 
const newFunction=(firstName,lastName)=>{
    return {
        firstName,
        lastName,
        fullname : () =>{
            console.log(`${firstName} ${lastName}`)
        }
    }
}

newFunction('nathania','calsita').fullname()

//Soal 3 
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName,lastName,address,hobby} = newObject

console.log(firstName,lastName,address,hobby)

//Soal 4 

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [ ...west , ...east]
//Driver Code
console.log(combined)

//Soal 5 
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after)

