<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth',
    'middleware'=>'api'
],function(){
    Route::post('register','RegisterController');
    Route::post('regenerate-otp-code','RegenerateOtpCodeController');
    Route::post('verification','VerificationController');
    Route::post('update-password','UpdatePasswordController');
    Route::post('login','LoginController');
});

Route::group([
    'prefix' =>'profile',
    'middleware'=>['email_verified','auth:api']
],function(){
    // Route::get('show','ShowProfileController');
    // Route::post('update-profile','EditProfileController');
    Route::get('show','ProfileController@show');
    Route::post('update','ProfileController@update');
});