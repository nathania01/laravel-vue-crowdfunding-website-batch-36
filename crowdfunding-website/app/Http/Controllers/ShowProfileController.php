<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class ShowProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $data['user'] = auth()->user();
        
        return response()->json([
            'data'=>$data
        ]);
    }
}
