<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class EditProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'photo_profile'=>'required|image|mimes:png,jpg,jpeg'
        ]);

        $photo_profile = $request->file('photo_profile')->store('profile');
        
        $user = auth()->user();
        $user->name = $request['name'];
        $user->photo_profile = $photo_profile;
        $user->save();

        return response()->json([
            'response_code'=> '00',
            'response_message' => 'Sukses update data!',
            'data'=> $user
        ]);
    }
}
