<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traints\UsesUuid;
use App\Role;
use App\Otp_code;
use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable, UsesUuid;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','email','name','password','role_id','photo_profile'
    ];
    protected $primaryKey = 'id';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Role langsung user 
    public static function boot(){
        parent::boot();
        static::creating(function($model){
            $model->role_id= $model->get_role_id();
        });
        static::created(function($model){
            $model->generate_otp_code();
        });
    }

    public function get_role_id(){
        $role = Role::where('name','user')->first();
        return $role->id;
    }

    public function generate_otp_code(){
        do{
            $random = mt_rand(100000,999999);
            $check = Otp_code::where('otp',$random)->first();
        }while($check);

        $now = Carbon::now();
        $otp_code = Otp_code::updateOrCreate(
            ['user_id'=>$this->id],
            [
                'otp'=>$random,
                'valid_until'=>$now->addMinute(5)
            ]
        );

    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    //Relasi
    public function role(){
        return $this->belongsTo('App\Role');
    }
    
    public function otpCode(){
        return $this->hasOne('App\Otp_code','user_id');
    }

}
